﻿set NAMES 'utf8';
USE depart;

-- producto cartesiano

  -- REALIZADO CON LA ,
  SELECT * 
    FROM depart d, emple e;

  -- REALIZADO CON JOIN
  SELECT * 
    FROM depart JOIN emple e1;


-- simular un join con el producto cartesiano
  SELECT * 
    FROM depart d, emple e 
    WHERE d.dept_no=e.dept_no;

-- utilizando inner join
-- con on
  SELECT * 
    FROM depart d 
      JOIN emple e ON d.dept_no = e.dept_no;

-- con using
-- los campos de combinacion tienen que llamarse igual
  SELECT * 
    FROM depart d JOIN emple e USING(dept_no);

-- con where
-- esto no vamos a utilizarlo
-- dejamos el where para realizar la seleccion
  
  -- opcion 1
  SELECT * 
    FROM depart d 
      JOIN emple e 
    WHERE d.dept_no = e.dept_no;

  -- opcion 2
  SELECT * 
    FROM depart d, emple e 
    WHERE d.dept_no = e.dept_no;


  -- COMBINACION EXTERNA

  -- LEFT JOIN  

  -- TODOS LOS DEPARTAMENTOS Y SOLO LOS EMPLEADOS QUE TENGAN UN DEPARTAMENTO
  -- ASIGNADO 

  SELECT 
      * 
    FROM depart d 
      LEFT JOIN emple e ON d.dept_no = e.dept_no;


  -- TODOS LOS EMPLEADOS Y SOLAMENTE LOS DEPARTAMENTOS QUE 
  -- TENGAN ALGUN EMPLEADO

  -- LEFT JOIN

SELECT 
    * 
  FROM emple e 
    LEFT JOIN depart d ON e.dept_no = d.dept_no;



-- FULL JOIN

  -- NO FUNCIONA
  SELECT 
      * 
    FROM emple e 
      FULL  JOIN depart d ON e.dept_no = d.dept_no;


  -- CON UNION
  SELECT 
      * 
    FROM emple e 
      LEFT JOIN depart d ON e.dept_no = d.dept_no
  
  UNION 
  
  SELECT 
      * 
    FROM emple e 
      RIGHT JOIN depart d ON e.dept_no = d.dept_no;

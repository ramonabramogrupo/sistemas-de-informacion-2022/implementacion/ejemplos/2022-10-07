﻿SET NAMES 'utf8';
USE ejemplo22;

-- combinacion interna

-- realizada con on
SELECT 
    * 
  FROM noticias n 
    JOIN autores a ON n.idAutor = a.idAutor;

-- realizado con using
-- solamente podemos utilizar using
-- si los campos de combinacion tienen el mismo nombre
SELECT 
    * 
  FROM noticias n 
    JOIN autores a USING(idAutor);


-- con producto cartesiano + where

-- opcion 1
SELECT 
    * 
  FROM noticias n, autores a 
  WHERE a.idAutor=n.idAutor;

-- opcion 2
SELECT 
    * 
  FROM noticias n JOIN autores a 
  WHERE a.idAutor=n.idAutor;


-- COMBINACION EXTERNA

-- LEFT JOIN CON ON
-- TODOS LOS AUTORES Y SOLAMENTE LAS NOTICIAS DE LAS CUALES CONOCES EL AUTOR
SELECT 
    * 
  FROM autores a 
    LEFT JOIN noticias n ON a.idAutor = n.idAutor;

-- LEFT JOIN CON USING
SELECT 
    * 
  FROM autores a 
    LEFT JOIN noticias n USING(idAutor);

-- LA MISMA CON RIGHT JOIN
SELECT 
    * 
  FROM noticias n 
    RIGHT JOIN autores a ON n.idAutor = a.idAutor;


-- LEFT JOIN
-- TODAS LAS NOTICIAS Y SOLAMENTE LOS AUTORES QUE HAYAN ESCRITO ALGUNA 
-- NOTICIA

SELECT 
    * 
  FROM noticias n 
    LEFT JOIN autores a ON n.idAutor = a.idAutor;


-- FULL JOIN
-- ESTE NO FUNCIONA
  SELECT 
      * 
    FROM noticias n 
      FULL JOIN autores a ON n.idAutor = a.idAutor;
  
 -- REALIZADO CON UNION
  SELECT 
      * 
    FROM noticias n 
      LEFT JOIN autores a ON n.idAutor = a.idAutor

  UNION 

  SELECT 
      * 
    FROM noticias n 
      RIGHT JOIN autores a ON n.idAutor = a.idAutor;


-- EJEMPLOS DE APLICACION

-- INDICAME LAS NOTICIAS QUE NO TIENEN AUTOR

  -- UTILIZANDO LAS DOS TABLAS
  SELECT 
      * 
    FROM noticias n 
      LEFT JOIN autores a ON n.idAutor = a.idAutor
    WHERE n.idAutor IS NULL;

  -- NO NECESITO AUTORES PARA RESOLVER LA CONSULTA
  SELECT 
      * 
    FROM noticias n
    WHERE n.idAutor IS NULL;


-- INDICAME LOS AUTORES QUE NO HAN ESCRITO NOTICIAS
-- AUTORES - AUTORES(NOTICIAS)
-- NECESITO LAS DOS TABLAS
  SELECT 
      * 
    FROM autores a 
      LEFT JOIN noticias n ON a.idAutor = n.idAutor
    WHERE n.idNoticia IS NULL;


    